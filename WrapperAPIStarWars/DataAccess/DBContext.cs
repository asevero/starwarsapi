﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace WrapperAPIStarWars.DataAccess
{
    public class StarwarsDbContext : DbContext
    {

        private static bool _created = false;


        public DbSet<CharacterRating> CharacterRatings { get; set; }

        public StarwarsDbContext()
        {
            if (!_created)
            {
                _created = true;
                Database.EnsureCreated();
            }
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = "starwarscharacters.db" };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);

            optionsBuilder.UseSqlite(connection);
        }
    }
}
