﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WrapperAPIStarWars.DataAccess
{
    [Table("CharacterRating")]
    public class CharacterRating
    {
        [Key]
        public int RatingId { get; set; }

        public int CharacterId { get; set; }

        [Range(1, 5)]
        public int Rating { get; set; }
    }
}
