﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;
using WrapperAPIStarWars.Business;
using WrapperAPIStarWars.DataAccess;
using WrapperAPIStarWars.Models;

namespace WrapperAPIStarWars.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/character")]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterManager ICharacterManager;
        private readonly IMemoryCache IMemoryCache;

        public CharacterController(
            IMemoryCache memoryCache,
            ICharacterManager characterManager)
        {
            IMemoryCache = memoryCache;
            ICharacterManager = characterManager;
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCharacter(int id)
        {
            if (id <= 0 || id > Int32.MaxValue)
            {
                return BadRequest(new { message = $"Invalid Character Id provided" });
            }

            CharacterResponse characterResponse = await GetCharacteryById(id);

            if (characterResponse == null)
                return BadRequest(new { message = $"Character with id \"{id}\" not found" });


            return Ok(characterResponse);
        }



        [HttpPost]
        [Route("{id}/rating")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostCharacter(int id, [FromForm] int rating)
        {
            if (rating < 1 || rating > 5)
            {
                return BadRequest(new { message = "Rating value is not valid, Only Rating from 1 to 5 are allowed" });
            }

            if (id < 1 || id > int.MaxValue)
            {
                return BadRequest(new { message = "Character Id is invalid" });
            }

            var character = await GetCharacteryById(id);


            if (character == null)
            {
                return BadRequest(new { message = "Character does not exist" });
            }


            var characterRating = new CharacterRating
            {
                Rating = rating,
                CharacterId = id
            };

            await ICharacterManager.AddNewRating(characterRating);


            // Remuevo el Item de la cache para generar la invalidacion y que vuelva a obtener el Character desde el API
            RemoveCacheItem(id);

            characterRating = null;
            character = null;

            return Ok(new
            {
                message = "Rating registered successfully"
            });
        }


        [NonAction]
        private async Task<CharacterResponse> GetCharacteryById(int id)
        {

            var characterMemoryCacheKey = $"character_{id}";

            if (!IMemoryCache.TryGetValue(characterMemoryCacheKey, out CharacterResponse _characterResponse))
            {
                _characterResponse = await ICharacterManager.GetCharacter(id);

                if (_characterResponse == null)
                {
                    return null;

                }

                var cacheEntryOptions = new MemoryCacheEntryOptions()  // Set cache options.                        
                      .SetSlidingExpiration(TimeSpan.FromMinutes(5));   // Keep in cache for this time, reset time if accessed.

                IMemoryCache.Set(characterMemoryCacheKey, _characterResponse, cacheEntryOptions); // Save data in cache.
            }

            return _characterResponse;
        }

        [NonAction]
        private void RemoveCacheItem(int id)
        {
            var characterMemoryCacheKey = $"character_{id}";

            try
            {
                IMemoryCache.Remove(characterMemoryCacheKey);
            }
            catch (Exception)
            {
                // TODO Loggear Exception

            }
        }
    }
}