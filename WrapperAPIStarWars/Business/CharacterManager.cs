﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WrapperAPIStarWars.DataAccess;
using WrapperAPIStarWars.Models;

namespace WrapperAPIStarWars.Business
{
    /// <summary>
    /// Manager de los Personajes de la saga Starwars
    /// </summary>
    public class CharacterManager : ICharacterManager
    {
        private readonly StarwarsDbContext dbContext;

        private readonly IConfiguration IConfiguration;

        private readonly string UrlSwapiEndpointBase;

        public CharacterManager(StarwarsDbContext starwarsDbContext, IConfiguration configuration)
        {
            IConfiguration = configuration;
            UrlSwapiEndpointBase = IConfiguration["Api:BaseUrl"];
            this.dbContext = starwarsDbContext;
        }


        /// <summary>
        /// Obtiene el Puntaje Mayor de Personaje
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns>Un valor definido entre 1 a 5</returns>
        public int MaxRatingById(int characterId)
        {
            var maxElements = dbContext.CharacterRatings
                .Where(x => x.CharacterId.Equals(characterId)).ToList();

            var maxRating = 0;

            if (maxElements.Any())
            {
                maxRating = maxElements.Max(x => x.Rating);
            }

            return maxRating;
        }


        /// <summary>
        ///  Retorna el Puntaje Promedio del Rating Recibido
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns>Un valor definido entre 1 a 5</returns>
        public double AverageRatingById(int characterId)
        {
            var averageElements = dbContext.CharacterRatings
                .Where(x => x.CharacterId.Equals(characterId)).ToList();


            var average = (double)0;

            if (averageElements.Any())
            {
                average = averageElements.Average(x => x.Rating);
            }

            return average;
        }

        /// <summary>
        /// Verifica si el Personaje existe en la base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True / False en caso de existir</returns>
        public bool CharacterExist(int id)
        {
            return dbContext.CharacterRatings.Any(x => x.CharacterId.Equals(id));
        }

        /// <summary>
        /// Agrega una nueva puntuacion a un Personaje
        /// </summary>
        /// <param name="characterRating">Objeto del Tipo CharacterRating</param>
        public async Task AddNewRating(CharacterRating characterRating)
        {
            dbContext.CharacterRatings.Add(characterRating);

            await dbContext.SaveChangesAsync();

            return;
        }

        public async Task<CharacterResponse> GetCharacter(int id)
        {
            var characterResponse = new CharacterResponse();

            var urlApiGetCharacter = $"{UrlSwapiEndpointBase}/people/{id}/";

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage prodResp = new HttpResponseMessage();

                    var hashEndpoint = $"character_{id}";

                    string responseAPI = await client.GetStringAsync(urlApiGetCharacter);

                    prodResp = await client.GetAsync(urlApiGetCharacter);

                    if (!prodResp.IsSuccessStatusCode)
                    {
                        return null;
                    }

                    var character = await prodResp.Content.ReadAsAsync<CharacterSwapiResponse>();

                    if (character != null)
                    {
                        characterResponse.name = character.name;
                        characterResponse.height = character.height;
                        characterResponse.mass = character.mass;
                        characterResponse.skin_color = character.skin_color;
                        characterResponse.hair_color = character.hair_color;
                        characterResponse.eye_color = character.eye_color;
                        characterResponse.birth_year = character.birth_year;
                        characterResponse.gender = character.gender;

                        // Obtengo Homeworld
                        var urlApiHomeWorld = character.homeworld;
                        var responseHome = await client.GetStringAsync(urlApiHomeWorld);
                        var worldSwapiResponse = JsonConvert.DeserializeObject<HomeWorldSwapiResponse>(responseHome);

                        characterResponse.homeworld = new Homeworld
                        {
                            name = worldSwapiResponse.name,
                            population = worldSwapiResponse.population,
                            known_residents_count = worldSwapiResponse.residents.Count()  // Tomo la cantidad de elementos
                        };

                        // Species
                        var urlApiSpecies = character.species.FirstOrDefault();

                        if (urlApiSpecies != null)
                        {
                            var responseSpecies = await client.GetStringAsync(urlApiSpecies);
                            var species = JsonConvert.DeserializeObject<Species>(responseSpecies);

                            // Nombre de la especie
                            characterResponse.species_name = species.name;
                        }

                        // Maxima puntuacion
                        characterResponse.max_rating = MaxRatingById(id);

                        // Promedio de puntuacion
                        characterResponse.average_rating = (int)AverageRatingById(id);

                    }

                    return characterResponse;

                }
                catch (Exception)
                {
                    // TODO:  Idealmente se podria tener un manejo de excepciones y incluir un Logger
                    return null;
                }
            }
        }
    }
}