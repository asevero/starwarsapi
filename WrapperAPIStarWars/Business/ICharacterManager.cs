﻿using System.Threading.Tasks;
using WrapperAPIStarWars.DataAccess;
using WrapperAPIStarWars.Models;

namespace WrapperAPIStarWars.Business
{
    public interface ICharacterManager
    {
        double AverageRatingById(int characterId);
        int MaxRatingById(int characterId);
        bool CharacterExist(int id);
        Task AddNewRating(CharacterRating characterRating);
        Task<CharacterResponse> GetCharacter(int id);
    }
}