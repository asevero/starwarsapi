﻿namespace WrapperAPIStarWars.Models
{
    public class CharacterResponse
    {
        public string name { get; set; }
        public string height { get; set; }
        public string mass { get; set; }
        public string hair_color { get; set; }
        public string skin_color { get; set; }
        public string eye_color { get; set; }
        public string birth_year { get; set; }
        public string gender { get; set; }
        public Homeworld homeworld { get; set; }
        public string species_name { get; set; }
        public int average_rating { get; set; }
        public int max_rating { get; set; }

        public CharacterResponse()
        {
        }

    }
}
