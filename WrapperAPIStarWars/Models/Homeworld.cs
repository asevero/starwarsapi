﻿namespace WrapperAPIStarWars.Models
{
    public class Homeworld
    {
        public string name { get; set; }

        public string population { get; set; }

        public int known_residents_count { get; set; }
    }
}
