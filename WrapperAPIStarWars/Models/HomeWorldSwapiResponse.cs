﻿namespace WrapperAPIStarWars.Models
{
    public class HomeWorldSwapiResponse
    {
        public string name { get; set; }

        public string population { get; set; }

        public string[] residents { get; set; }
    }
}
